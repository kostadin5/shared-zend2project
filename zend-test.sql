-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zend-test`
--

-- --------------------------------------------------------

--
-- Структура на таблица `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=218 ;

--
-- Схема на данните от таблица `album`
--

INSERT INTO `album` (`id`, `artist`, `title`, `ord`) VALUES
(1, 'The Military Wives', 'In My Dreams', 2),
(2, 'Adele', '21', 1),
(3, 'Bruce Springsteen', 'Wrecking Ball (Deluxe)', 3),
(4, 'Lana Del Rey', 'Born To Die', 8),
(5, 'Gotye', 'Making Mirrors', 6),
(6, 'John', 'Arizona MM', 4),
(7, 'Али баба и', '40-те разбойника', 5),
(8, 'v', 'b', 7),
(9, 'sse', 'asa', 9),
(12, 'dd', 'ss2', 10),
(168, 'Kodaline', 'The High Hopes EP', 23),
(169, 'Lana Del Rey', 'Born to Die', 24),
(170, 'JAY Z & Kanye West', 'Watch the Throne (Deluxe Version)', 25),
(171, 'Biffy Clyro', 'Opposites', 26),
(172, 'Various Artists', 'Return of the 90s', 27),
(173, 'Gabrielle Aplin', 'Please Don''t Say You Love Me - EP', 28),
(174, 'Various Artists', '100 Hits - Driving Rock', 29),
(175, 'Jimi Hendrix', 'Experience Hendrix - The Best of Jimi Hendrix', 30),
(176, 'Various Artists', 'The Workout Mix 2013', 31),
(177, 'The 1975', 'Sex', 32),
(178, 'Chase & Status', 'No More Idols', 33),
(179, 'Rihanna', 'Unapologetic (Deluxe Version)', 34),
(180, 'The Killers', 'Battle Born', 35),
(181, 'Olly Murs', 'Right Place Right Time (Deluxe Edition)', 36),
(182, 'A$AP Rocky', 'LONG.LIVE.A$AP (Deluxe Version)', 37),
(183, 'Various Artists', 'Cooking Songs', 38),
(184, 'Haim', 'Forever - EP', 39),
(185, 'Lianne La Havas', 'Is Your Love Big Enough?', 40),
(186, 'Michael Bubl', 'To Be Loved', 41),
(187, 'Daughter', 'If You Leave', 42),
(188, 'The xx', 'xx', 43),
(189, 'Eminem', 'Curtain Call', 44),
(190, 'Kendrick Lamar', 'good kid, m.A.A.d city (Deluxe)', 45),
(191, 'Disclosure', 'The Face - EP', 46),
(192, 'Palma Violets', '180', 47),
(193, 'Cody Simpson', 'Paradise', 48),
(194, 'Ed Sheeran', '+ (Deluxe Version)', 49),
(195, 'Michael Bubl', 'Crazy Love (Hollywood Edition)', 50),
(196, 'Bon Jovi', 'Bon Jovi Greatest Hits - The Ultimate Collection', 51),
(197, 'Rita Ora', 'Ora', 53),
(198, 'g33k', 'Spabby', 54),
(199, 'Various Artists', 'Annie Mac Presents 2012', 55),
(200, 'David Bowie', 'The Platinum Collection', 58),
(201, 'Bridgit Mendler', 'Ready or Not (Remixes) - EP', 56),
(202, 'Dido', 'Girl Who Got Away', 52),
(203, 'Various Artists', 'Now That''s What I Call Disney', 59),
(204, 'The 1975', 'Facedown - EP', 57),
(205, 'Kodaline', 'The Kodaline - EP', 11),
(206, 'Various Artists', '100 Hits: Super 70s', 12),
(207, 'Fred V & Grafix', 'Goggles - EP', 13),
(208, 'Biffy Clyro', 'Only Revolutions (Deluxe Version)', 14),
(209, 'Train', 'California 37', 15),
(210, 'Ben Howard', 'Every Kingdom (Deluxe Edition)', 16),
(211, 'Various Artists', 'Motown Anthems', 18),
(212, 'Courteeners', 'ANNA', 19),
(213, 'Johnny Marr', 'The Messenger', 22),
(214, 'Rodriguez', 'Searching for Sugar Man', 20),
(215, 'Jessie Ware', 'Devotion', 21),
(216, 'Bruno Mars', 'Unorthodox Jukebox', 17),
(217, '60', '60', 60);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
