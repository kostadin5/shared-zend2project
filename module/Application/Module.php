<?php

namespace Application;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\Model\Album;
use Application\Model\AlbumTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $sessionContainer = new \Zend\Session\Container('locale');

        if (!isset($sessionContainer->language)) {
            if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $h1 = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
                $hx = explode(",", $h1);
                $hsize = sizeof($hx);
                $flag = false;
                for ($i = 0; $i < $hsize; $i++) {
                    $hsss = substr($hx[$i], 0, 2);
                    if (!$flag) {
                        switch ($hsss) {
                            case 'bg':
                                $flag = true;
                                $sessionContainer->language = 'bg';
                                break;
                            case 'en':
                                $flag = true;
                                $sessionContainer->language = 'en';
                                break;
                            case 'es':
                                $flag = true;
                                $sessionContainer->language = 'es';
                                break;
                        }
                    }
                }
                if (!$sessionContainer->language)
                    $sessionContainer->language = 'bg';
            }
        }
        switch ($sessionContainer->language) {
            case 'bg':
                $translator->setLocale('bg_BG');
                break;
            case 'en':
                $translator->setLocale('en_US');
                break;
            case 'es':
                $translator->setLocale('es_ES');
                break;
            default:
                $translator->setLocale('bg_BG');
                $sessionContainer->language = 'bg';
        }
    }

    public function getConfig() {
        return include __dir__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(__namespace__ =>
                    __dir__ . '/src/' . __namespace__,),
                ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Application\Model\AlbumTable' => function ($sm) {
                    $tableGateway = $sm->get('ApplicationTableGateway');
                    $table = new AlbumTable($tableGateway);
                    return $table;
                },
                'ApplicationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Album());
                    return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
