<?php 
namespace Application\Form;
use Zend\Form\Form;
use Zend\Form\Element;

class AlbumForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('album');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
            'attributes' => array(
                'placeholder' => 'Add a title...',
                'id' => 'test_id1',
                'class' => 'form-control',
                'style' => 'width: 160px;',
                'required' => 'required',
             ),
            'options' => array('label' => 'Title', ),
        ));
        $this->add(array(
            'name' => 'artist',
            'type' => 'Text',
            'attributes' => array(
                'placeholder' => 'Add an artist..',
                'required' => 'required',
                'class' => 'form-control',
                'style' => 'width: 160px;',
                ),
            'options' => array('label' => 'Artist', ),
        ));
        $this->add(array(
            'name' => 'ord',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'id' => 'submitbutton',
                'class' => 'btn btn-success btn-sm',
            ),
        ));
    }
}
