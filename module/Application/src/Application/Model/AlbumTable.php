<?php 

namespace Application\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class AlbumTable
{

    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = false)
    {
        if ($paginated)
        {
            $select = new Select('album');
            $select->order('ord ASC');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Album());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype);
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select(function ($select){
            $select->order('ord ASC'); }
        );
        return $resultSet;
    }

    public function getAlbum($id){
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row)
        {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveAlbum(Album $album)
    {
        $ord = (int)$album->ord;
        if ($ord == 0){
            $album->ord = (int)$this->getMax();
            $album->ord = $album->ord + 1;
        }
        $data = array(
            'artist' => $album->artist,
            'title' => $album->title,
            'ord' => $album->ord,
        );

        $id = (int)$album->id;
        if ($id == 0){
            $this->tableGateway->insert($data);
        } 
        else{
            if ($this->getAlbum($id)){
                $this->tableGateway->update($data, array('id' => $id));
            } 
            else{
                throw new \Exception('Album id does not exist');
            }
        }
    }
    public function sortAlbum(Album $album){
        $ord = (int)$album->ord;
        if ($ord == 0)
        {
            $album->ord = (int)$this->getMax();
            $album->ord = $album->ord + 1;
        }
        $data = array(
            'ord' => $album->ord,
        );
        $id = (int)$album->id;
        if ($this->getAlbum($id)){
            $this->tableGateway->update($data, array('id' => $id));
        } 
        else{
            throw new \Exception('Album id does not exist');
        }
    }
    public function deleteAlbum($id){
        $this->tableGateway->delete(array('id' => (int)$id));
    }
    public function getMax(){
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('ord' => new Expression('MAX(`ord`)')));
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row)
        {
            throw new \Exception("Could not retrieve max Publication nr");
        }
        return $row->ord;
    }
}
