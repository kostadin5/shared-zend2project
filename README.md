Console application for creating the Multiplication Table.

Requirements:

PHP 5.5.* or above
Composer

For first instalation:

1. UnZip it to desired location.
2. Use "composer install" to install dependentcy
3. Create database whit name zend-test and import zend-test.sql to it
4. Create virtual host like: zf2.localhost

Setting up the virtual host is usually done within httpd.conf or extra/httpd-vhosts.conf. 
If you are using httpd-vhosts.conf, ensure that this file is included by your main httpd.conf file. 
Some Linux distributions (ex: Ubuntu) package Apache so that configuration files are stored in /etc/apache2 
and create one file per virtual host inside folder /etc/apache2/sites-enabled. 
In this case, you would place the virtual host block below into the file /etc/apache2/sites-enabled/zf2-tutorial.

For example add folowing to your apache virtual hosts to point to the public/ directory of the project:

    <VirtualHost *:80>
        ServerName zf2.localhost
        DocumentRoot /path/to/zf2/public
        SetEnv APPLICATION_ENV "development"
        <Directory /path/to/zf2/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>

Make sure that you update your /etc/hosts or c:\windows\system32\drivers\etc\hosts file so that zf2.localhost is mapped to 127.0.0.1. 
The website can then be accessed using http://zf2.localhost

